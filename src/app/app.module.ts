import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DirectivesRenderComponentModule } from './pages/directives-render-component/directives-render-component.module';
import { DataPickerComponent } from './pages/data-picker/data-picker.component';

@NgModule({
  declarations: [
    AppComponent,
    DataPickerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DirectivesRenderComponentModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
