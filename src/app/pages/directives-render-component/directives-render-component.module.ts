import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DirectivesRenderComponentComponent } from './directives-render-component.component';
import { DirectiveRenderDirective } from './directive-render.directive';
import { DirectiveRenderDirectiveLegacy } from './directive-render.legacy.directive';



@NgModule({
  declarations: [
    DirectivesRenderComponentComponent,
    DirectiveRenderDirective,
    DirectiveRenderDirectiveLegacy,
  ],
  imports: [
    CommonModule
  ],
  exports:[
    DirectivesRenderComponentComponent,
    DirectiveRenderDirective,
    DirectiveRenderDirectiveLegacy,
  ]
})
export class DirectivesRenderComponentModule { }
