import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-render-component',
  templateUrl: './directives-render-component.component.html',
  styleUrls: ['./directives-render-component.component.scss']
})
export class DirectivesRenderComponentComponent implements OnInit {
  tooltip: string = 'Qualquer coisa';
  left: number = 0;
  top: number = 0;

  constructor() { }

  ngOnInit(): void {

  }
}
