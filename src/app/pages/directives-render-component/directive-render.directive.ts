import { ApplicationRef, ComponentFactoryResolver, ComponentRef, Directive, ElementRef, EmbeddedViewRef, HostListener, Injector, Input, ViewContainerRef } from '@angular/core';
import { DirectivesRenderComponentComponent } from './directives-render-component.component';

@Directive({
  selector: '[appDirectiveRender]'
})
export class DirectiveRenderDirective {
  @Input() tooltip = '';
  private componentRef: ComponentRef<DirectivesRenderComponentComponent> | null = null;
  constructor(
    /**
     * elementRef - we will use it to access the HTML element on which we place the directive tag, eg. a button or a link, to check its position in the browser window;
     */
    private elementRef: ElementRef<HTMLElement>,
    /**
     * appRef - reference to a whole app, that will allow us to inject our tooltip to the application HTML content globally;
     */
    private appRef: ApplicationRef,
    /**
     * componentFactoryResolver - that will provide us with a factory for creating our TooltipComponent programmatically inside the TooltipDirective code;
     */
    private viewContainerRef: ViewContainerRef,
  ) { }

  @HostListener('mouseenter') onMouseEnter(): void {
    if (this.componentRef === null) {

      //#region CriaComponent
      // Usando o viewContainerRef ele além de ja criar o componente ele também já o vincula na DOM.
      this.componentRef = this.viewContainerRef.createComponent(DirectivesRenderComponentComponent);
      //#endregion CriaComponent

      this._setTooltipComponentProperties();
    }
  }

  @HostListener('mouseleave') onMouseLeave(): void {
    this.destroy();
  }

  ngOnDestroy(): void {
    this.destroy();
  }

  destroy(): void {
    if (this.componentRef !== null) {
      this.appRef.detachView(this.componentRef.hostView);
      this.componentRef.destroy();
      this.componentRef = null;
    }
  }

  private _setTooltipComponentProperties(): void {
    if (this.componentRef !== null) {
      this.componentRef.instance.tooltip = this.tooltip;
      /** Pega a posição do elemento na dom */
      const { left, right, bottom } = this.elementRef.nativeElement.getBoundingClientRect();
      this.componentRef.instance.left = (right - left) / 2 + left;
      this.componentRef.instance.top = bottom;
    }
  }
}
